%define __global_requires_exclude_from  /usr/libexec

Summary: AI auto tuning system
Name: atune
Version: 1.2.0
Release: 2
License: MulanPSL-2.0
URL: https://gitee.com/openeuler/A-Tune
Source: https://gitee.com/openeuler/A-Tune/repository/archive/v%{version}.tar.gz

Patch9000: A-Tune-Add-sw64-architecture.patch
Patch9001: add-riscv64-support.patch
Patch9002: remove-the-installation-of-atune-ui-related-files.patch
Patch9003: adjust-the-startup-sequence-of-atune-rest.patch
Patch9004: import-os-for-app.py.patch 
Patch9005: adapt-sqlite-3.42-to-resolve-the-build-failure.patch

BuildRequires: rpm-build golang-bin procps-ng
BuildRequires: sqlite >= 3.24.0 openssl
BuildRequires: python3-scikit-optimize python3-pandas python3-xgboost
BuildRequires: python3-pyyaml
BuildRequires: systemd
BuildRequires: perf
Requires: systemd
Requires: atune-client = %{version}-%{release}
Requires: atune-db = %{version}-%{release}
Requires: python3-dict2xml
Requires: python3-flask-restful
Requires: python3-pandas
Requires: python3-pyyaml
%ifarch aarch64
Requires: prefetch_tuning
%endif
Requires: perf
Requires: sysstat
Requires: hwloc-gui
Requires: psmisc
Requires: atune-collector
Requires: atune-rest

%define  debug_package %{nil}

%description
atune is a service for atuned AI tuning system.

%package client
Summary: client tool for auto tuning system
License: MuLan PSL v2

%description client
atune client tool for manage atuned AI tuning system.

%package db
Summary: database and AI model for auto tuning system
License: MuLan PSL v2

%description db
Database and AI model used by atuned AI tuning system.

%package engine
Summary: engine tool for auto tuning system
License: MuLan PSL v2
Requires: python3-scikit-optimize
Requires: python3-xgboost
Requires: python3-flask-restful
Requires: python3-pandas
Requires: python3-lhsmdu
Conflicts: atune < 1.1.0

%description engine
atune engine tool for manage atuned AI tuning system.

%package rest
Summary: restful api for communication between atuned and atune-engine
License: MuLan PSL v2
Conflicts: atune < 1.1.0

%description rest
atune restful api for manage atuned AI tuning system.

%prep
%setup -n A-Tune-v%{version}
%ifarch sw_64
%patch9000 -p1
%endif
%ifarch riscv64
%patch9001 -p1
%endif
%patch9002 -p1
%patch9003 -p1
%patch9004 -p1
%patch9005 -p1

%build
%make_build

%install
%make_install

%check

%post
%systemd_post atuned.service

%preun
%systemd_preun atuned.service

%postun
%systemd_postun_with_restart atuned.service

%post engine
%systemd_post atune-engine.service

%preun engine
%systemd_preun atune-engine.service

%postun engine
%systemd_postun_with_restart atune-engine.service

%post rest
%systemd_post atune-rest.service

%preun rest
%systemd_preun atune-rest.service

%postun rest
%systemd_postun_with_restart atune-rest.service

%files
%license License/LICENSE
%defattr(0640,root,root,0750)
%attr(0550,root,root) /usr/lib/atuned/modules/daemon_profile_server.so
%attr(0640,root,root) %{_unitdir}/atuned.service
%attr(0550,root,root) %{_bindir}/atuned
/usr/lib/atuned/profiles/*
%attr(0750,root,root) %dir /usr/lib/atuned
%attr(0550,root,root) %dir /usr/lib/atuned/modules
%attr(0750,root,root) %dir /usr/lib/atuned/profiles
%attr(0750,root,root) %dir /usr/libexec/atuned
%attr(0750,root,root) %dir /usr/share/atuned
%attr(0750,root,root) %dir /etc/atuned
%attr(0750,root,root) %dir /etc/atuned/rules
%attr(0750,root,root) %dir /etc/atuned/tuning
%attr(0750,root,root) %dir /var/atuned
%attr(0640,root,root) /etc/atuned/atuned.cnf
%attr(0700,root,root) %dir /etc/atuned/engine_certs
%attr(0700,root,root) %dir /etc/atuned/rest_certs
%exclude /etc/atuned/engine_certs/*
%exclude /etc/atuned/rest_certs/*

%files rest
%attr(0550,root,root) %dir /usr/libexec/atuned/analysis
%attr(0550,root,root) /usr/libexec/atuned/analysis/*
%exclude /usr/libexec/atuned/analysis/app_engine.py
%exclude /usr/libexec/atuned/analysis/models/
%exclude /usr/libexec/atuned/analysis/optimizer/
%exclude /usr/libexec/atuned/analysis/engine/
%exclude /usr/libexec/atuned/analysis/dataset/
%attr(0640,root,root) %{_unitdir}/atune-rest.service

%files client
%attr(0550,root,root) %{_bindir}/atune-adm
%attr(0550,root,root) /usr/share/bash-completion/completions/atune-adm

%files db
%attr(0750,root,root) %dir /var/lib/atuned
%attr(0750,root,root) %dir /var/run/atuned
%attr(0640,root,root) /var/lib/atuned/atuned.db
%attr(0750,root,root) %dir /usr/libexec/atuned
%attr(0550,root,root) %dir /usr/libexec/atuned/analysis
%attr(0550,root,root) %dir /usr/libexec/atuned/analysis/models
%attr(0550,root,root) /usr/libexec/atuned/analysis/models/*

%files engine
%license License/LICENSE
%defattr(0640,root,root,0750)
%attr(0640,root,root) %{_unitdir}/atune-engine.service
%attr(0550,root,root) /usr/libexec/atuned/analysis/*
/etc/atuned/*
%exclude /usr/libexec/atuned/analysis/app_rest.py
%exclude /usr/libexec/atuned/analysis/plugin/
%exclude /usr/libexec/atuned/analysis/atuned/
%attr(0550,root,root) %dir /usr/libexec/atuned/analysis
%attr(0750,root,root) %dir /etc/atuned
%exclude /etc/atuned/atuned.cnf
%exclude /etc/atuned/rules
%exclude /etc/atuned/tuning
%attr(0700,root,root) %dir /etc/atuned/engine_certs
%exclude /etc/atuned/engine_certs/*
%exclude /etc/atuned/rest_certs

%changelog
* Mon Mar 11 2024 zhoupengcheng <zhoupengcheng11@huawei.com> - 1.2.0-2
- add import and fix build failure

* Tue Feb 6 2024 zhoupengcheng <zhoupengcheng11@huawei.com> - 1.2.0-1
- update atune-v1.2.0

* Wed Nov 8 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 1.0.0-16
- fix-collection-train-file-overwriting-through-soft-links

* Wed Nov 8 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 1.0.0-15
- define-fix-privilege-escalation

* Tue Oct 17 2023 sunchendong <sunchendong@xfusion.com> - 1.0.0-14
- atune update Makefile and logs

* Fri Aug 25 2023 sunchendong <sunchendong@xfusion.com> - 1.0.0-13
- atune add service restart mode

* Thu Aug 10 2023 gaoruoshu <gaoruoshu@huawei.com> - 1.0.0-12
- bugfix set collection/transfer/train dir to specific dir

* Wed Aug 2 2023 zhangxiang <zhangxiang@iscas.ac.cn> - 1.0.0-11
- add riscv64 support

* Fri Apr 21 2023 yuxiangyang <yuxiangyang4@xfusion.com> - 1.0.0-10
- fix atune-engine process remaining after remove atune-engine.rpm

* Fri Mar 3 2023 wuzx<wuzx1226@qq.com> - 1.0.0-9
- Add sw64 architecture

* Sat Jan 28 2023 panxiaohe <pan_xiaohe@hoperun.com> - 1.0.0-8
- fix build failure that unitdir cannot be identified

* Thu Aug 11 2022 gaoruoshu <gaoruoshu@huawei.com> - 1.0.0-7
- add whitespace to version

* Tue Aug 09 2022 gaoruoshu <gaoruoshu@huawei.com> - 1.0.0-6
- specifies the version number

* Fri Aug 05 2022 gaoruoshu <gaoruoshu@huawei.com> - 1.0.0-5
- fix start failed of atuned service

* Fri Aug 05 2022 fushanqing <fushanqing@kylinos.cn> - 1.0.0-4
- Unified license name specification

* Fri Mar 04 2022 Bin Hu <hubin73@huawei.com> - 1.0.0-3
- change Makefile A-Tune version to v1.0.0

* Fri Mar 04 2022 gaoruoshu <gaoruoshu@huawei.com> - 1.0.0-2
- enable certificate authentication by default and modify file permissions

* Tue Nov 16 2021 hanxinke <hanxinke@huawei.com> - 1.0.0-1
- upgrade to v1.0.0

* Tue Sep 28 2021 hanxinke <hanxinke@huawei.com> - 0.3-0.9
- add successExitStatus 100 to atuned.service

* Sat Mar 20 2021 BruceGW <gyl93216@163.com> - 0.3-0.8
- add go compile flag

* Mon Jan 11 2021 gaoruoshu <gaoruoshu@huawei.com> - 0.3-0.7
- update file description and test case

* Wed Dec 30 2020 HW_TaoChen <boby.chen@huawei.com> - 0.3-0.6
- support for go 1.15

* Sat Nov 28 2020 hanxinke<hanxinke@huawei.com> - 0.3-0.5
- The engine package conflicts with atune < 0.3-0.1.

* Mon Nov 9 2020 hanxinke<hanxinke@huawei.com> - 0.3-0.4
- fix wrong license

* Wed Sep 9 2020 Zhipeng Xie<xiezhipeng1@huawei.com> - 0.3-0.3
- only require prefetch_tuning on aarch64

* Mon Sep 7 2020 Zhipeng Xie<xiezhipeng1@huawei.com> - 0.3-0.2
- fix wrong requires

* Fri Sep 4 2020 Zhipeng Xie<xiezhipeng1@huawei.com> - 0.3-0.1
- upgrade to v0.3

* Thu Mar 19 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.2-0.1
- Package init

* Tue Nov 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.1-0.1
- Package init

